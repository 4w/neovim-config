# This WIP repository was moved

The repository was moved away from GitLab. The new location can be found here:

* <https://git.0x7be.net/dirk/neovim-config>

Just set the new origin in your local copy of the repository.

```bash
# Check the current origin
git remote show origin url

# Set the new origin
git remote set-url origin https://git.0x7be.net/dirk/neovim-config.git

# Confirm that setting the new origin worked
git remote show origin url
```

The configuration did not change. It is still in actively maintained in a public Git repository. All that has changed is the public Git hosting location.
